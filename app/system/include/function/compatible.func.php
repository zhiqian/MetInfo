<?php
# MetInfo Enterprise Content Management System
# Copyright (C) MetInfo Co.,Ltd (http://www.metinfo.cn). All rights reserved.

defined('IN_MET') or exit('No permission');

/**
 * @param string $path 要包含的模板文件地址，已“模板文件类型/模板文件名称”方式输入
 * @模板文件类型：own:应用自己的模板文件，ui:系统UI模板文件，tem:模板文件
 * @除前台模板文件外，其他包含的文件一定是php格式
 * @param $path
 * @return string
 */
function template($path)
{
    global $_M;
    //替换系统Url
    $web_site = $_M['url']['web_site'];
    $url = array();
    foreach ($_M['url'] as $key => $val) {
        if (in_array($key, array('web_site', 'admin_site'))) {
            $url[$key] = $val;
        } else {
            $val = str_replace($web_site, '../', $val);
            $url[$key] = $val;
        }
    }
    $_M['url'] = $url;

    // 前缀、路径转换优化（新模板框架v2）
    $dir = explode('/', $path);
    $postion = $dir[0];
    $file = substr(strstr($path, '/'), 1);

    if ($postion == 'own') {
        return PATH_OWN_FILE . "templates/{$file}.php";
    }
    if ($postion == 'ui') {
        if (M_MODULE == 'admin') {
            $ui = 'admin_old';
        } else {
            $ui = 'web';
        }

        return PATH_SYS_TEM . "{$ui}/{$file}.php";
    }
    if ($postion == 'tem') {
        if (M_MODULE == 'admin') {
            if (file_exists(PATH_SYS . '/' . M_NAME . "/admin/templates/{$file}.php")) {
                return PATH_SYS . '/' . M_NAME . "/admin/templates/{$file}.php";
            } else {
                return PATH_SYS . "index/admin/templates/{$file}.php";
            }
        } else {
            $tem_w = 'php';
            if ($_M['form']['ajax'] == 1) {
                $file_ajax = 'ajax/' . $file;
                if (file_exists(PATH_TEM . "{$file_ajax}.php")) {
                    return PATH_TEM . "{$file_ajax}.php";
                }
                if (file_exists(PATH_TEM . "{$file_ajax}.html")) {
                    return PATH_TEM . "{$file_ajax}.html";
                }
                if (file_exists(PATH_TEM . "{$file_ajax}.htm")) {
                    return PATH_TEM . "{$file_ajax}.htm";
                }
                if (file_exists(PATH_PUBLIC_WEB . "templates/{$file_ajax}.{$tem_w}")) {
                    return PATH_PUBLIC_WEB . "templates/{$file_ajax}.{$tem_w}";
                }
            }
            if (file_exists(PATH_TEM . "{$file}.php")) {
                return PATH_TEM . "{$file}.php";
            }
            if (file_exists(PATH_TEM . "{$file}.html")) {
                return PATH_TEM . "{$file}.html";
            }
            if (file_exists(PATH_TEM . "{$file}.htm")) {
                return PATH_TEM . "{$file}.htm";
            }

            return PATH_PUBLIC_WEB . "templates/{$file}.{$tem_w}";
        }
    }
}
# This program is an open source system, commercial use, please consciously to purchase commercial license.
# Copyright (C) MetInfo Co., Ltd. (http://www.metinfo.cn). All rights reserved.
?>